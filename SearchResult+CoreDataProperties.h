//
//  SearchResult+CoreDataProperties.h
//  
//
//  Created by Raul Alonso Moreno on 03/9/17.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SearchResult.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchResult (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *country;
@property (nullable, nonatomic, retain) NSDate *lastVisited;
@property (nullable, nonatomic, retain) Coordinate *coordinate;

@end

NS_ASSUME_NONNULL_END
