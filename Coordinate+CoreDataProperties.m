//
//  Coordinate+CoreDataProperties.m
//  
//
//  Created by Raul Alonso Moreno on 03/9/17.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Coordinate+CoreDataProperties.h"

@implementation Coordinate (CoreDataProperties)

@dynamic north;
@dynamic south;
@dynamic east;
@dynamic west;
@dynamic latitude;
@dynamic longitude;

@end
