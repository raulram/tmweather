//
//  DataCRUD.m
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import "DataCRUD.h"
#import "SearchResult.h"
#import "Coordinate.h"
#import "AppDelegate.h"

@implementation DataCRUD

+(id)searchResultWithName:(NSString*)name country:(NSString*)country North:(NSString*)cNorth South:(NSString*)cSouth East:(NSString*)cEast West:(NSString*)cWest latitude:(NSString*)lat longitude:(NSString*)lng{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    SearchResult *result = [NSEntityDescription
                        insertNewObjectForEntityForName:@"SearchResult"
                        inManagedObjectContext:context];
    
    Coordinate *coord = [NSEntityDescription
                            insertNewObjectForEntityForName:@"Coordinate"
                            inManagedObjectContext:context];
    
    [coord setNorth:cNorth];
    [coord setSouth:cSouth];
    [coord setEast:cEast];
    [coord setWest:cWest];
    [coord setLatitude:lat];
    [coord setLongitude:lng];
    [result setName:name];
    [result setCountry:country];
    [result setCoordinate:coord];
    [result setLastVisited:[NSDate date]];
    
    [appDelegate saveContext];
    
    return result;
}

+ (void) updateLastVisitedDateToResult:(SearchResult*)result{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [result setLastVisited:[NSDate date]];
    [appDelegate saveContext];
}

+(NSArray*)getLastSearchResults
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:@"SearchResult" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastVisited" ascending:NO];
    
    [request setEntity:entityDescription];
    [request setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (array == nil)
    {
        // Controlamos los posibles errores
    }
    
    return array;
}



@end
