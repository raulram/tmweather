//
//  DataCRUD.h
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SearchResult.h"

@interface DataCRUD : NSObject


+(id)searchResultWithName:(NSString*)name country:(NSString*)country North:(NSString*)cNorth South:(NSString*)cSouth East:(NSString*)cEast West:(NSString*)cWest latitude:(NSString*)lat longitude:(NSString*)lng;

+(NSArray*)getLastSearchResults;

+ (void) updateLastVisitedDateToResult:(SearchResult*)result;
@end
