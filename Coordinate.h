//
//  Coordinate.h
//  
//
//  Created by Raul Alonso Moreno on 03/9/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Coordinate : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Coordinate+CoreDataProperties.h"
