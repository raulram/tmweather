//
//  MainViewController.h
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MainViewController : UIViewController<MKMapViewDelegate>

/**
 *  search button outlet
 */
@property (weak, nonatomic) IBOutlet UIBarButtonItem *searchButton;

/**
 *  Map outlet
 */
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

/**
 *  Open right side menu
 *
 *  @param sender button
 */
- (IBAction)openRightView:(id)sender;

@end
