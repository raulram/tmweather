//
//  RESTClient.h
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

#define URL_API @"http://api.geonames.org" 
#define API_USERNAME @"ilgeonamessample"

typedef enum _APIOperationId
{
    APIOperationSearch = 0,
    APIOperationWeather = 1,
} APIOperationId;

@interface RESTClient : AFHTTPSessionManager

- (void)getSerchResultsWithQuery:(NSString*)query OnSuccess:(void (^)(APIOperationId, NSDictionary*))success failure:(void (^)(NSError *error))failure;

- (void)getWeatherInfoWithCoordinatesNorth:(NSString*)cNorth south:(NSString*)cSouth east:(NSString*)cEast west:(NSString*)cWest OnSuccess:(void (^)(APIOperationId, NSDictionary*))success failure:(void (^)(NSError *error))failure;

@end
