//
//  SideViewController.m
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import "SideViewController.h"

@interface SideViewController ()


@end

@implementation SideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    
    [self setRootViewController:navigationController];
    [self setup];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


/**
 *  setup right controller
 */
- (void)setup{
    _rightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RightViewController"];
    
    [self setRightViewEnabledWithWidth:self.view.frame.size.width * 0.8
                     presentationStyle:LGSideMenuPresentationStyleSlideBelow
                  alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];
    
    self.rightViewStatusBarStyle = UIStatusBarStyleDefault;
    self.rightViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnNone;
    [self.rightView addSubview:_rightViewController.tableView];
}


/**
 *  set side menu size
 *
 *  @param size current size
 */
- (void)rightViewWillLayoutSubviewsWithSize:(CGSize)size
{
    [super rightViewWillLayoutSubviewsWithSize:size];
    _rightViewController.tableView.frame = CGRectMake(0.f , 0.f, size.width, size.height);
}


@end
