//
//  RESTModel.m
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import "RESTModel.h"
#import "JSONModel.h"


@implementation BoxModel
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end

@implementation GeoNamesModel
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end

@implementation SearchResultModel
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end

@implementation WeatherObservationsModel
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end

@implementation WeatherResultModel
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end
