//
//  MainViewController.m
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import "MainViewController.h"
#import "SideViewController.h"
#import "RESTModel.h"
#import "APIMgr.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "MapViewAnnotation.h"
#import "PNChart.h"
#import "DataCRUD.h"
#import "SearchResult.h"
#import "Coordinate.h"

#define SCREEN_HEIGHT    ([UIScreen mainScreen].bounds.size.height)

@interface MainViewController (){
    SideViewController* svc;
    PNCircleChart * meanTemperatureChart;
    PNBarChart * temperatureChart;
    UIView *chartBackgroundView;
}

@end

@implementation MainViewController

/**
 *  Load method
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    
    svc = (SideViewController *)[UIApplication sharedApplication].delegate.window.rootViewController;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideSidePenlGesture:)
                                                 name:kLGSideMenuControllerWillDismissRightViewNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(locationSelectedAction:)
                                                 name:@"LocationSelected"
                                               object:nil];
    
    [self createCharts];
    
}

/**
 *  view will appear
 *
 *  @param animated show animated
 */
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

/**
 *  memory warning
 */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


/**
 *  Method called to open side menu
 *
 *  @param sender
 */
- (IBAction)openRightView:(id)sender {
    
    if ([svc isRightViewShowing]){
        [svc hideRightViewAnimated:YES completionHandler:nil];
        self.mapView.userInteractionEnabled = YES;
        self.searchButton.title = @"Buscar";
    }
    else{
        [svc showRightViewAnimated:YES completionHandler:nil];
        self.mapView.userInteractionEnabled = NO;
        self.searchButton.title = @"Ocultar";
    }
}

/**
 *  Detect when the user close side menu
 *
 *  @param notification gesture
 */
- (void) hideSidePenlGesture:(NSNotification *) notification
{
    self.mapView.userInteractionEnabled = YES;
    self.searchButton.title = @"Buscar";
    [svc.rightViewController.locSearchBar endEditing:YES];
}

/**
 *  Notification listener to detect the location selected by user
 *
 *  @param notification last-visited/search-locations list
 */
-(void) locationSelectedAction:(NSNotification *) notification
{
    [self.mapView removeAnnotations:self.mapView.annotations];
    if ([notification.object isKindOfClass:[GeoNamesModel class]])
    {
        GeoNamesModel *itemSelected = [notification object];
        if (itemSelected.bbox != nil) {
            [self getWeatherInfoForBoxInNorth:itemSelected.bbox.north South:itemSelected.bbox.south East:itemSelected.bbox.east West:itemSelected.bbox.west];
            self.title = itemSelected.name;
            [self addCityToMapOnLatitude:itemSelected.lat longitude:itemSelected.lng withName:itemSelected.name];
        }
    }
    else if ([notification.object isKindOfClass:[SearchResult class]]){
        SearchResult *itemSelected = [notification object];
        [self getWeatherInfoForBoxInNorth:itemSelected.coordinate.north South:itemSelected.coordinate.south East:itemSelected.coordinate.east West:itemSelected.coordinate.west];
        self.title = itemSelected.name;
        [self addCityToMapOnLatitude:itemSelected.coordinate.latitude longitude:itemSelected.coordinate.longitude withName:itemSelected.name];
    }
    else
    {
        NSLog(@"Error, object not recognised.");
    }
    [svc hideRightViewAnimated:YES completionHandler:nil];
}

#pragma mark - API Functions

/**
 *  Function to get weather info by coordinates
 *
 *  @param cNorth coordinate
 *  @param cSouth coordinate
 *  @param cEast  coordinate
 *  @param cWest  coordinate
 */
- (void) getWeatherInfoForBoxInNorth:(NSString*)cNorth South:(NSString*)cSouth East:(NSString*)cEast West:(NSString*)cWest{
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    
    [[APIMgr sharedInstance] getWeatherInfoWithCoordinatesNorth:cNorth
                                                          south:cSouth
                                                           east:cEast
                                                           west:cWest
                                                      OnSuccess:^(APIOperationId operation, JSONModel* data)
     {
         WeatherResultModel *api_result = (WeatherResultModel*)data;
         [self addObservatorsAnnotationsWithData:api_result];
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         [self refreshChartsWithData:api_result];
         
         
     } failure:^(NSError *error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
     }];
}

#pragma mark - Map Functions

/**
 *  Add selected city pin to map
 *
 *  @param lat  latitude
 *  @param lng  longitude
 *  @param name city name
 */
- (void) addCityToMapOnLatitude:(NSString*)lat longitude:(NSString*)lng withName:(NSString*)name
{
    CLLocationCoordinate2D track;
    track.latitude = [lat doubleValue];
    track.longitude = [lng doubleValue];
    
    MapViewAnnotation *newAnnotation = [[MapViewAnnotation alloc] initWithTitle:name AndCoordinate:track];
    
    [self.mapView addAnnotation:newAnnotation];

}

/**
 *  Adds observators pins to map
 *
 *  @param data api response data
 */
- (void) addObservatorsAnnotationsWithData:(WeatherResultModel*)data{
    for (WeatherObservationsModel *observator in data.weatherObservations) {
        
        CLLocationCoordinate2D track;
        track.latitude = [observator.lat doubleValue];
        track.longitude = [observator.lng doubleValue];
        NSString *text = [NSString stringWithFormat:@"%@%@%@%@",observator.stationName,@" - ", observator.temperature,@"ºC"];
        MapViewAnnotation *newAnnotation = [[MapViewAnnotation alloc] initWithTitle:text AndCoordinate:track];
        [self.mapView addAnnotation:newAnnotation];
    }
    
    [self zoomToFitMapAnnotations:self.mapView];
}

/**
 *  center map to include all pins
 *
 *  @param mapView current map
 */
-(void)zoomToFitMapAnnotations:(MKMapView*)mapView
{
    if([mapView.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(MapViewAnnotation* annotation in mapView.annotations)
    {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.6;
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.1;
    
    region = [mapView regionThatFits:region];
    [mapView setRegion:region animated:YES];
}


#pragma mark - Chart Functions

/**
 *  Create charts objects
 */
-(void)createCharts{
    
    CGFloat barHeight = 110;
    CGFloat barChartHeight = 150;
    CGFloat circleChartSize = 75;
    CGFloat circleChartX = 10;
    CGFloat circleChartY = 26;
    CGFloat barChartWidth = SCREEN_WIDTH - circleChartSize - 20;
    CGFloat barChartX = circleChartX + circleChartSize;
    CGFloat barChartY = -30;

    meanTemperatureChart = [[PNCircleChart alloc] initWithFrame:CGRectMake(circleChartX, circleChartY, circleChartSize, circleChartSize)
                                                          total:[NSNumber numberWithInt:50]
                                                        current:[NSNumber numberWithInt:0]
                                                      clockwise:YES
                                                         shadow:NO
                                                    shadowColor:[UIColor lightGrayColor]];
    meanTemperatureChart.backgroundColor = [UIColor clearColor];
    meanTemperatureChart.chartType = PNChartFormatTypeNone;
    meanTemperatureChart.countingLabel.textColor = [UIColor whiteColor];
    meanTemperatureChart.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
                                             UIViewAutoresizingFlexibleTopMargin);
    
    
    temperatureChart = [[PNBarChart alloc] initWithFrame:CGRectMake(barChartX, barChartY, barChartWidth, barChartHeight)];
    temperatureChart.backgroundColor = [UIColor clearColor];
    temperatureChart.showLabel = NO;
    temperatureChart.labelTextColor = [UIColor grayColor];
    temperatureChart.labelFont = [UIFont systemFontOfSize:6];
    temperatureChart.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
                                             UIViewAutoresizingFlexibleTopMargin);
    
    chartBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - barHeight, SCREEN_WIDTH, barHeight)];
    chartBackgroundView.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.65];
    chartBackgroundView.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
                                         UIViewAutoresizingFlexibleTopMargin);
    


}

/**
 *  Update charts
 *
 *  @param data data to show in charts
 */
- (void)refreshChartsWithData:(WeatherResultModel*)data{
    
    NSNumber *meanTemperature = [self getMeanTemperatureWithData:data];
    if (meanTemperature != nil) {

        [meanTemperatureChart updateChartByCurrent:meanTemperature];
        [meanTemperatureChart setStrokeColor:[UIColor clearColor]];
        [meanTemperatureChart setStrokeColorGradientStart:[self getStrokeColorForTemparature:meanTemperature.intValue]];
        [meanTemperatureChart strokeChart];
        meanTemperatureChart.displayAnimated = YES;
        [chartBackgroundView addSubview:meanTemperatureChart];
        
        [self setValuesToChart:temperatureChart withData:data];
        [temperatureChart strokeChart];
        temperatureChart.displayAnimated = YES;
        [chartBackgroundView addSubview:temperatureChart];
        
        [self.view addSubview:chartBackgroundView];   
        
    }
    else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.detailsLabelText = @"No hay datos para la ubicación seleccionada.";
        hud.margin = 10.f;
        hud.yOffset = 150;
        hud.removeFromSuperViewOnHide = YES;
        [meanTemperatureChart removeFromSuperview];
        [temperatureChart removeFromSuperview];
        [chartBackgroundView removeFromSuperview];
        [hud hide:YES afterDelay:3.0];

    }
    
}

/**
 *  set x, y and colors to chart
 *
 *  @param chart chart
 *  @param data  data to set
 */
- (void) setValuesToChart:(PNBarChart*)chart withData:(WeatherResultModel*)data{
   
    NSMutableArray *xLabelsArray = [[NSMutableArray alloc]init];
    NSMutableArray *yLabelsArray = [[NSMutableArray alloc]init];
    NSMutableArray *strokeColorsArray = [[NSMutableArray alloc]init];
    
    for (WeatherObservationsModel *observator in data.weatherObservations) {
        NSString* auxTmp = [NSString stringWithFormat:@"%d",[observator.temperature intValue]];
        [yLabelsArray addObject:auxTmp];
        [xLabelsArray addObject:observator.stationName];
        [strokeColorsArray addObject:[self getStrokeColorForTemparature:[observator.temperature intValue]]];
    }
    
    [temperatureChart setStrokeColors:strokeColorsArray];
    [temperatureChart setXLabels:xLabelsArray];
    [temperatureChart setYValues:yLabelsArray];

}

/**
 *  Get mean temperature from observators data
 *
 *  @param data data info
 *
 *  @return return mean temperature
 */
- (NSNumber*) getMeanTemperatureWithData:(WeatherResultModel*)data{

    float totalValue = 0;
    int count = 0;
    
    for (WeatherObservationsModel *observator in data.weatherObservations) {
        totalValue += [observator.temperature intValue];
        count++;
    }
    
    if (count == 0) {
        return nil;
    }
    
    totalValue = totalValue/count;
    return [NSNumber numberWithInt:totalValue];
}

/**
 *  get stroke color for temperature value
 *
 *  @param value temperature value
 *
 *  @return color
 */
- (UIColor*)getStrokeColorForTemparature:(int)value{
    if (value > 20) {
        return [UIColor redColor];
    }
    else if (value > 15){
        return [UIColor orangeColor];
    }
    else if (value > 10){
        return [UIColor yellowColor];
    }
    else{
        return [UIColor blueColor];
    }
}

@end
