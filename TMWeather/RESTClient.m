//
//  RESTClient.m
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import "RESTClient.h"


/**
 *  Use AFNetworking to call web services (API REST)
 */
@implementation RESTClient


/**
 *  API call to search web service
 *
 *  @param query   query to search
 *  @param success success
 *  @param failure failure
 */
- (void)getSerchResultsWithQuery:(NSString*)query OnSuccess:(void (^)(APIOperationId, NSDictionary*))success failure:(void (^)(NSError *error))failure
{
    
    NSString *urlServer = URL_API;
    NSString *resourcePath = [NSString stringWithFormat:@"%@%@%@%@",@"/searchJSON?q=",query,@"&maxRows=20&startRow=0&lang=en&isNameRequired=true&style=FULL&username=",API_USERNAME];
    NSString *url = [NSString stringWithFormat:@"%@%@", urlServer, resourcePath];
    //http://api.geonames.org/searchJSON?q=Madrid&maxRows=20&startRow=0&lang=en&isNameRequired=true&style=FULL&username=ilgeonamessample

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        success(APIOperationSearch, responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        failure(error);
    }];

}

/**
 *  API call to weather info
 *
 *  @param cNorth
 *  @param cSouth
 *  @param cEast
 *  @param cWest
 *  @param success
 *  @param failure 
 */
- (void)getWeatherInfoWithCoordinatesNorth:(NSString*)cNorth south:(NSString*)cSouth east:(NSString*)cEast west:(NSString*)cWest OnSuccess:(void (^)(APIOperationId, NSDictionary*))success failure:(void (^)(NSError *error))failure
{
    
    NSString *urlServer = URL_API;
    NSString *resourcePath = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@",@"/weatherJSON?north=", cNorth, @"&south=", cSouth, @"&east=", cEast, @"&west=", cWest, @"&username=", API_USERNAME];
    NSString *url = [NSString stringWithFormat:@"%@%@", urlServer, resourcePath];
    //http://api.geonames.org/weatherJSON?north=44.1&south=-9.9&east=-22.4&west=55.2&username=demo
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        success(APIOperationWeather, responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        failure(error);
    }];

    
}

@end
