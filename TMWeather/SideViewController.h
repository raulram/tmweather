//
//  SideViewController.h
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGSideMenuController.h"
#import "RightViewController.h"

@interface SideViewController : LGSideMenuController

@property (strong, nonatomic) RightViewController *rightViewController;

- (void)setup;

@end
