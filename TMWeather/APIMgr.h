//
//  APIMgr.h
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RESTClient.h"
#import "JSONModel.h"

@interface APIMgr : NSObject

+ (APIMgr*)sharedInstance;

- (void)getSerchResultsWithQuery:(NSString*)query OnSuccess:(void (^)(APIOperationId, JSONModel*))success failure:(void (^)(NSError *error))failure;

- (void)getWeatherInfoWithCoordinatesNorth:(NSString*)cNorth south:(NSString*)cSouth east:(NSString*)cEast west:(NSString*)cWest OnSuccess:(void (^)(APIOperationId, JSONModel*))success failure:(void (^)(NSError *error))failure;

@end
