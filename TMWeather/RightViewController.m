//
//  RightViewController.m
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import "RightViewController.h"
#import "RESTModel.h"
#import "APIMgr.h"
#import "DataCRUD.h"
#import "SearchResult.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface RightViewController ()
@property (strong, nonatomic) NSArray *searchResults;
@property (strong, nonatomic) NSArray *lastResults;
@end


@implementation RightViewController

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"image"]];
    self.searchDisplayController.searchResultsTableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"image"]];
    
    _searchResults = [[NSMutableArray alloc]init];
    _lastResults = [DataCRUD getLastSearchResults];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)searchDisplayController:(UISearchController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self search:searchString];
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self search:searchBar.text];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    _lastResults = [DataCRUD getLastSearchResults];
    [self.searchDisplayController setActive:NO animated:YES];
    [self.tableView reloadData];
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableView) {
        return _lastResults.count;
        
    } else {
        return _searchResults.count;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self.tableView)
    {
        return @"Últimos visitados";
    }
    else
    {
        return @"Resultado de la busqueda";
    }

}

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selected %ld row", (long)indexPath.row);
    [self.locSearchBar endEditing:YES];

    if (theTableView == self.tableView)
    {
        SearchResult *selectedItem = [_lastResults objectAtIndex:indexPath.row];
        [DataCRUD updateLastVisitedDateToResult:selectedItem];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationSelected" object:selectedItem];
    }
    else{
        GeoNamesModel *selectedItem = [_searchResults objectAtIndex:indexPath.row];
        [DataCRUD searchResultWithName:selectedItem.name country:selectedItem.countryName North:selectedItem.bbox.north South:selectedItem.bbox.south East:selectedItem.bbox.east West:selectedItem.bbox.west latitude:selectedItem.lat longitude:selectedItem.lng];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationSelected" object:selectedItem];
    }
    
    [self searchBarCancelButtonClicked:nil];
    [self.tableView deselectRowAtIndexPath:theTableView.indexPathForSelectedRow animated:YES];
    
    
}


#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    }
    
    NSString *name;
    NSString *country;
    if (tableView == self.tableView)
    {
        SearchResult *item = [_lastResults objectAtIndex:indexPath.row];
        name = item.name;
        country = item.country;
    }
    else
    {
        GeoNamesModel *item = [_searchResults objectAtIndex:indexPath.row];
        name = item.name;
        country = item.countryName;
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont systemFontOfSize:16.f];
    cell.textLabel.textColor =  [UIColor whiteColor];
    cell.textLabel.text = [NSString stringWithFormat:@"%@%@%@%@",name,@" (", country,@")"];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.f;
}


#pragma mark - API Functions

/**
 *  get cities results from query
 *
 *  @param query query search string
 */
- (void) search:(NSString*)query{
    [[APIMgr sharedInstance] getSerchResultsWithQuery:query OnSuccess:^(APIOperationId operation, JSONModel* data) {
        SearchResultModel *api_result = (SearchResultModel*)data;
        _searchResults = api_result.geonames;
        [self.searchDisplayController.searchResultsTableView reloadData];

    } failure:^(NSError *error) {
    }];
}

@end
