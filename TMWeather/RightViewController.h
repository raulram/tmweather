//
//  RightViewController.h
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RightViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UISearchBar *locSearchBar;

@end
