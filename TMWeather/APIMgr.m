//
//  APIMgr.m
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import "APIMgr.h"
#import "RESTModel.h"

/**
 *  Maps API funtions results to Objects (RESTKit)
 */
@implementation APIMgr{
    RESTClient *restClient;
}

- (id)init
{
    self = [super init];
    if (self) {
        restClient = [[RESTClient alloc] init];
    }
    return self;
}

/**
 *  Get API Manager singleton
 *
 *  @return singleton
 */
+ (APIMgr*)sharedInstance
{
    
    static APIMgr *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[APIMgr alloc] init];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    });
    
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        
    }];
    
    return _sharedInstance;
}

/**
 *  Parse search API function result to object
 *
 *  @param query
 *  @param success
 *  @param failure
 */
- (void)getSerchResultsWithQuery:(NSString*)query OnSuccess:(void (^)(APIOperationId, JSONModel*))success failure:(void (^)(NSError *error))failure
{
    [restClient getSerchResultsWithQuery:query OnSuccess:^(APIOperationId operation, NSDictionary* data) {
        NSError* err = nil;
        SearchResultModel *api_result = [[SearchResultModel alloc] initWithDictionary:data error:&err];
        if (err != nil) {
            failure(err);
            NSLog(@"error: search api parse");
        }
        else{
            success(operation, api_result);
            NSLog(@"success: search api parse");
        }
    } failure:^(NSError *error) {
        failure(error);
        NSLog(@"failure: search api parse");
    }];
}


/**
 *  Parse get weather API function to object
 *
 *  @param cNorth
 *  @param cSouth
 *  @param cEast
 *  @param cWest
 *  @param success
 *  @param failure
 */
- (void)getWeatherInfoWithCoordinatesNorth:(NSString*)cNorth south:(NSString*)cSouth east:(NSString*)cEast west:(NSString*)cWest OnSuccess:(void (^)(APIOperationId, JSONModel*))success failure:(void (^)(NSError *error))failure
{

    [restClient getWeatherInfoWithCoordinatesNorth:cNorth south:cSouth east:cEast west:cWest OnSuccess:^(APIOperationId operation, NSDictionary* data) {
        NSError* err = nil;
        WeatherResultModel *api_result = [[WeatherResultModel alloc] initWithDictionary:data error:&err];
        if (err != nil) {
            failure(err);
            NSLog(@"error: get weather api parse");
        }
        else{
            success(operation, api_result);
            NSLog(@"success: get weather api parse");
        }

    } failure:^(NSError *error) {
        failure(error);
        NSLog(@"failure: get weather api parse");
    }];
}

@end
