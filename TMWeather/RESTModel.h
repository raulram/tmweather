//
//  RESTModel.h
//  TMWeather
//
//  Created by Raul Alonso Moreno on 03/9/17.
//  Copyright © 2017 ram. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

/**
 *  Box Model
 */
@interface BoxModel : JSONModel
@property (strong, nonatomic) NSString* east;
@property (strong, nonatomic) NSString* south;
@property (strong, nonatomic) NSString* north;
@property (strong, nonatomic) NSString* west;
@end

/**
 *  GeoNames mdel
 */
@protocol GeoNamesModel
@end
@interface GeoNamesModel : JSONModel
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* countryName;
@property (strong, nonatomic) NSString* lat;
@property (strong, nonatomic) NSString* lng;
@property (strong, nonatomic) BoxModel<Optional>* bbox;
@end

/**
 *  City search result model
 */
@interface SearchResultModel : JSONModel
@property (assign, nonatomic) int totalResultsCount;
@property (strong, nonatomic) NSArray<GeoNamesModel, Optional>* geonames;
@end


/**
 *  Weather observations model
 */
@protocol WeatherObservationsModel
@end
@interface WeatherObservationsModel : JSONModel
@property (strong, nonatomic) NSString* clouds;
@property (strong, nonatomic) NSString* temperature;
@property (strong, nonatomic) NSString* humidity;
@property (strong, nonatomic) NSString* windSpeed;
@property (strong, nonatomic) NSString* stationName;
@property (strong, nonatomic) NSString* lat;
@property (strong, nonatomic) NSString* lng;
@end

/**
 *  Weather result model
 */
@interface WeatherResultModel : JSONModel
@property (strong, nonatomic) NSArray<WeatherObservationsModel, Optional>* weatherObservations;
@end
