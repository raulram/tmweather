//
//  SearchResult+CoreDataProperties.m
//  
//
//  Created by Raul Alonso Moreno on 03/9/17.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SearchResult+CoreDataProperties.h"

@implementation SearchResult (CoreDataProperties)

@dynamic name;
@dynamic country;
@dynamic lastVisited;
@dynamic coordinate;

@end
