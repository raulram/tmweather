//
//  SearchResult.h
//  
//
//  Created by Raul Alonso Moreno on 03/9/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Coordinate;

NS_ASSUME_NONNULL_BEGIN

@interface SearchResult : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "SearchResult+CoreDataProperties.h"
