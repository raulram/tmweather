//
//  Coordinate+CoreDataProperties.h
//  
//
//  Created by Raul Alonso Moreno on 03/9/17.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Coordinate.h"

NS_ASSUME_NONNULL_BEGIN

@interface Coordinate (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *north;
@property (nullable, nonatomic, retain) NSString *south;
@property (nullable, nonatomic, retain) NSString *east;
@property (nullable, nonatomic, retain) NSString *west;
@property (nullable, nonatomic, retain) NSString *latitude;
@property (nullable, nonatomic, retain) NSString *longitude;

@end

NS_ASSUME_NONNULL_END
